select Country.Name from
Country inner join City on Country.Code = City.CountryCode
group by Country.Name
having sum(City.Population)/1.0/Country.Population < 0.5
;
