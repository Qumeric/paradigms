select Country.Name, Country.Population, Country.SurfaceArea from
Country inner join (City inner join Capital on City.Id = Capital.CityId)
on Country.Code = City.CountryCode
where City.Population not in
(select max(City.Population) from
Country inner join City on Country.Code = City.CountryCode
group by Country.Code)
order by Country.Population/1.0/Country.SurfaceArea desc, Country.Name
;
