def verbing(s):
    """Given a string, if its length is at least 3,
    add 'ing' to its end.
    Unless it already ends in 'ing', in which case
    add 'ly' instead.
    If the string length is less than 3, leave it unchanged.
    Return the resulting string."""
    if len(s) < 3:
        return s
    if s[-3:] == 'ing':
        return s + 'ly'
    return s + 'ing'


def not_bad(s):
    """Given a string, find the first appearance of the
    substring 'not' and 'bad'. If the 'bad' follows
    the 'not', replace the whole 'not'...'bad' substring
    with 'good'.
    Return the resulting string."""
    a = s.find('not')
    b = s.find('bad')
    if a != -1 and b != -1 and b > a:
        return s[:a] + 'good' + s[b+3:]
    return s


def front_back(a, b):
    """Consider dividing a string into two halves.
    If the length is even, the front and back halves are the same length.
    If the length is odd, we'll say that the extra char goes in the front half.
    e.g. 'abcde', the front half is 'abc', the back half 'de'.
    Given 2 strings, a and b, return a string of the form
    a-front + b-front + a-back + b-back"""
    a_mid = len(a) // 2 + len(a) % 2
    b_mid = len(b) // 2 + len(b) % 2
    return a[:a_mid] + b[:b_mid] + a[a_mid:] + b[b_mid:]

if __name__ == '__main__':
    verb = input()
    print(verbing(verb))
    text = input()
    print(not_bad(text))
    string1 = input()
    string2 = input()
    print(front_back(string1, string2))
