import sys
import os
import hashlib
from collections import defaultdict


def get_hash(f):
    BLOCKSIZE = 65536
    hasher = hashlib.sha1()
    buf = f.read(BLOCKSIZE)
    while buf:
        hasher.update(buf)
        buf = f.read(BLOCKSIZE)
    return hasher.hexdigest()


def find_duplicates(folder):
    samefiles = defaultdict(list)

    for root, _, files in os.walk(folder):
        for filename in files:
            if filename[0] == '.' or filename[-1] == '~' or \
                    os.path.islink(filename):
                continue
            fullname = os.path.abspath(os.path.join(root, filename))
            with open(fullname, 'rb') as f:
                samefiles[get_hash(f)].append(fullname)
    for files in samefiles.values():
        if len(files) > 1:
            print(":".join(files))


if __name__ == '__main__':
    if len(sys.argv) > 1:
        find_duplicates(sys.argv[1].rstrip('/'))
    else:
        print("Usage: ./duplicates.py folder")
