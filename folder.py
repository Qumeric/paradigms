import model
from model import *

class ConstantFolder:
    def visit(self, t):
        nt = []
        for node in t:
            if type(node) != list and type(node) not in inspect.getmembers(model): # mysterious
                continue
            if type(node) == UnaryOperation:
                nt.append(self.optimizeUnary(node))
            elif type(node) == BinaryOperation:
                nt.append(self.optimizeBinary(node))
            elif type(node) == list:
                nt.append([self.visit(x) for x in node])
            else:
                for var in node.__dict__.value():
                    nt.append(self.visit(getattr(node, value)))
        return nt

    def optimizeUnary(self, unary):
        scope = Scope()
        if type(unary.expr) == Number:
            return unary.evaluate(scope)

    def optimizeBinary(self, binary):
        scope = Scope()
        if type(binary.lhs) == Number and type(binary.rhs) == Number:
            return binary.evaluate(scope)

        if (binary.op == '*' and
                ((type(binary.lhs) == Number and binary.lhs.value == 0) or
                (type(binary.rhs) == Number and binary.rhs.value == 0))):
            return Number(0)

        if binary.op == '-' and type(binary.lhs) == Reference and \
                type(binary.rhs) == Reference and \
                binary.lhs.name == binary.rhs.name:
            return Number(0)


if __name__ == '__main__':
    folder = ConstantFolder()
    scope = Scope()
    Read('a').evaluate(scope)
    a = [BinaryOperation(Number(1), '+', Number(2))]
    b = [UnaryOperation('-', Number(1))]
    c = [BinaryOperation(Number(0), '*', Number(11))]
    d = [BinaryOperation(Reference('a'), '-', Reference('a'))]
    a = folder.visit(a)
    b = folder.visit(b)
    c = folder.visit(c)
    d = folder.visit(d)
    print(a[0].value)
    print(b[0].value)
    print(c[0].value)
    print(d[0].value)
