def remove_adjacent(lst):
    """Remove equal adjacent elements"""
    return lst[:1] + [lst[x] for x in range(1, len(lst)) if lst[x] != lst[x-1]]


def linear_merge(lst1, lst2):
    """Merge two sorted lists in one sorted list in linear time"""
    lst3 = []
    i = j = 0
    while i < len(lst1) and j < len(lst2):
        if lst1[i] <= lst2[j]:
            lst3.append(lst1[i])
            i += 1
        else:
            lst3.append(lst2[j])
            j += 1
    return lst3 + lst1[i:] + lst2[j:]

if __name__ == '__main__':
    lst = input().split()
    print(remove_adjacent(lst))
    lst1 = input().split()
    lst2 = input().split()
    print(linear_merge(lst1, lst2))
