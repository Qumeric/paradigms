module BinaryTree where
import Prelude hiding (lookup)

data BinaryTree k v = Empty | BinaryTree k v (BinaryTree k v) (BinaryTree k v) deriving (Show)

lookup :: Ord k => k -> BinaryTree k v -> Maybe v
lookup _ Empty = Nothing
lookup x (BinaryTree k v l r)
    | x == k = Just v
    | x < k  = lookup x l
    | x > k  = lookup x r

merge :: Ord k => BinaryTree k v -> BinaryTree k v -> BinaryTree k v
merge t Empty = t
merge Empty t = t
merge (BinaryTree k1 v1 l1 r1) (BinaryTree k2 v2 l2 r2)
    | k1 == k2 = merge (merge l1 r1) (BinaryTree k2 v2 l2 r2)
    | k1 < k2  = BinaryTree k1 v1 l1 (merge r1 (BinaryTree k2 v2 l2 r2))
    | k1 > k2  = BinaryTree k1 v1 (merge l1 (BinaryTree k2 v2 l2 r2)) r1

insert :: Ord k => k -> v -> BinaryTree k v -> BinaryTree k v
insert x y Empty = BinaryTree x y Empty Empty
insert x y (BinaryTree k v l r)
    | x == k = BinaryTree x y l r
    | x < k  = BinaryTree k v (insert x y l) r
    | x > k  = BinaryTree k v l (insert x y r)


delete :: Ord k => k -> BinaryTree k v -> BinaryTree k v
delete _ Empty = Empty
delete x (BinaryTree k v l r)
    | x == k = merge l r
    | x < k  = BinaryTree k v (delete x l) r
    | x > k  = BinaryTree k v l (delete x r)

l = BinaryTree 2 11 Empty Empty
r = BinaryTree 4 1111 Empty Empty
tree = BinaryTree 3 111 l r

t = BinaryTree 5 11111 tree (BinaryTree 6 111111 Empty Empty)
