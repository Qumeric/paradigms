from model import *

class PrettyPrinter:

    def __init__(self):
        self.level = 0
        self.tabsize = 4

    def print(self, string):
        if '}' in string: self.level -= self.tabsize
        print(' ' * self.level + string)
        if '{' in string: self.level += self.tabsize

    def visit(self, tree):
        return getattr(self, "print" + type(tree).__name__)(tree)

    def get(self, tree):
        return getattr(self, "get" + type(tree).__name__)(tree)

    def getNumber(self, number):
        return str(number.value)

    def getPrint(self, prt):
        return "print " + self.get(prt.expr)

    def getRead(self, read):
        return "read " + read.name

    def getReference(self, reference):
        return reference.name

    def getUnaryOperation(self, operation):
        return operation.op + self.get(operation.expr)

    def getBinaryOperation(self, operation):
        return ("(" + self.get(operation.lhs) + " " + operation.op + " "
                    + self.get(operation.rhs) + ")")
    def getFunctionCall(self, functionCall):
        argslist = [self.get(arg) for arg in functionCall.args]
        return self.get(functionCall.fun_expr) + "(" + ", ".join(argslist) + ")"

    def printNumber(self, number):
        self.print(self.getNumber(number) + ';')

    def printPrint(self, prt):
        self.print(self.getPrint(prt) + ';')

    def printRead(self, read):
        self.print(self.getRead(read) + ';')

    def printReference(self, ref):
        self.print(self.getReference(ref) + ';')

    def printUnaryOperation(self, unary):
        self.print(self.getUnaryOperation(unary) + ';')

    def printBinaryOperation(self, binary):
        self.print(self.getBinaryOperation(binary) + ';')

    def printFunctionCall(self, functionCall):
        self.print(self.getFunctionCall(functionCall) + ';')

    def printConditional(self, conditional):
        self.print("if (" + self.get(conditional.condition) + ") {")

        if conditional.if_true:
            for expr in conditional.if_true:
                self.visit(expr)

        if conditional.if_false:
            self.print("} else {")
            for expr in conditional.if_false:
                self.visit(expr)

        self.print('};')

    def printFunctionDefinition(self, functionDefinition):
        self.print("def " + functionDefinition.name + "(" +
            ', '.join(functionDefinition.function.args) + ") {")

        for statement in functionDefinition.function.body:
            self.visit(statement)

        self.print('};')

if __name__ == '__main__':
    scope = Scope()
    Read('a').evaluate(scope)
    Read('b').evaluate(scope)
    number = Number(42)
    prt = Print(number)
    unary = UnaryOperation('-', number)
    binary = BinaryOperation(number, "+", number)
    read = Read("x")
    reference = Reference("z")
    conditional1 = Conditional(number, [unary], [binary])
    conditional2 = Conditional(number, None)
    call = FunctionCall(reference, [number, number])

    printer = PrettyPrinter()
    printer.visit(number)
    printer.visit(read)
    printer.visit(prt)
    printer.visit(reference)
    printer.visit(unary)
    printer.visit(binary)
    printer.visit(conditional1)
    printer.visit(conditional2)
    printer.visit(call)


    gcd = Function(['a', 'b'], [
                    Conditional(
                    UnaryOperation('!',
                                   BinaryOperation(Reference('a'),
                                                   '&&',
                                                   Reference('b'))),
                    [Conditional(Reference('a'),
                                 [Reference('a')],
                                 [Reference('b')])],
                    [Conditional(BinaryOperation(Reference('a'),
                                                 '>=',
                                                 Reference('b')),
                                 [FunctionCall(Reference('gcd'),
                                               [BinaryOperation(
                                                Reference('a'),
                                                '%',
                                                Reference('b')),
                                                Reference('b')])],
                                 [FunctionCall(Reference('gcd'),
                                               [Reference('a'),
                                                BinaryOperation(
                                                Reference('b'),
                                                '%',
                                                Reference('a'))])])])])

    p = Function(['a', 'b'], [
            Conditional(
                UnaryOperation('!', Reference('b')),
                [Reference('1')],
                [Conditional(
                    BinaryOperation(
                        Reference('b'),
                        '%',
                        Reference('2')
                    ),
                    [BinaryOperation(
                        FunctionCall(Reference('power'), [
                            Reference('a'),
                            BinaryOperation(
                                Reference('b'),
                                '-',
                                Reference('1')
                            )
                        ]),
                        '*',
                        Reference('a')
                    )],
                    [BinaryOperation(
                        FunctionCall(Reference('power'), [
                            Reference('a'),
                            BinaryOperation(
                                Reference('b'),
                                '//',
                                Reference('2')
                            )
                        ]),
                        '*',
                        FunctionCall(Reference('power'), [
                            Reference('a'),
                            BinaryOperation(
                                Reference('b'),
                                '//',
                                Reference('2')
                            )
                        ])
                    )]
                )]
            )
        ])

    print("---HERE GOES GCD---")
    printer.visit(FunctionDefinition('gcd', gcd))
    print("---HERE GOES P")
    printer.visit(FunctionDefinition('p', p))
