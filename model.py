class Scope(object):
    def __init__(self, parent = None):
        self.parent = parent
        self.hashmap = {}

    def __getitem__(self, key):
        if key.isnumeric():
            return Number(int(key)) # added feature FIXME seems useless?
        if key in self.hashmap:
            return self.hashmap[key]
        if self.parent:
            return self.parent[key]

    def __setitem__(self, key, value):
        self.hashmap[key] = value

class Number:
    def __init__(self, value):
        self.value = value

    def evaluate(self, scope):
        return self

class Function:
    def __init__(self, args, body):
        self.args = args[:]
        self.body = body[:]

    def evaluate(self, scope):
        retval = None
        for f in self.body:
            retval = f.evaluate(scope)
        return retval

class FunctionDefinition:
    def __init__(self, name, function):
       self.name = name
       self.function = function

    def evaluate(self, scope):
        scope[self.name] = self.function
        return self.function

class Conditional:
    def __init__(self, condition, if_true, if_false = None):
        self.condition = condition
        self.if_true = if_true
        self.if_false = if_false

    def evaluate(self, scope):
        if self.condition.evaluate(scope).value:
            self.value = self.if_true
        else:
            self.value = self.if_false

        if self.value:
            retval = None
            for f in self.value:
                retval = f.evaluate(scope)
            return retval

class Print:
    def __init__(self, expr):
        self.expr = expr

    def evaluate(self, scope):
        print(self.expr.evaluate(scope).value)

class Read:
    def __init__(self, name):
        self.name = name

    def evaluate(self, scope):
        scope[self.name] = Number(int(input()))

class FunctionCall:
    def __init__(self, fun_expr, args):
        self.fun_expr = fun_expr
        self.args = args

    def evaluate(self, scope):
        function = self.fun_expr.evaluate(scope)
        call_args = [a.evaluate(scope) for a in self.args]
        call_scope = Scope(scope)
        for a, b in zip(function.args, call_args):
            call_scope[a] = b
        return function.evaluate(call_scope)

class Reference:
    def __init__(self, name):
        self.name = name

    def evaluate(self, scope):
        return scope[self.name]

class BinaryOperation:
    def __init__(self, lhs, op, rhs):
        self.lhs = lhs
        self.op = op
        self.rhs = rhs

    def evaluate(self, scope):
        lhs_ev = self.lhs.evaluate(scope)
        rhs_ev = self.rhs.evaluate(scope)
        op = ' ' + self.op + ' '
        if op == ' && ': op = ' and '
        if op == ' || ': op = ' or '
        # DEBUG print("Binary eval: ", str(lhs_ev.value) + op + str(rhs_ev.value))
        return Number(eval(str(lhs_ev.value) + op + str(rhs_ev.value))) # sv_cheats 1

class UnaryOperation:
    def __init__(self, op, expr):
        self.op = op
        self.expr = expr

    def evaluate(self, scope):
        expr_ev = self.expr.evaluate(scope)
        if self.op == '-':
            return Number(-expr_ev.value)
        if expr_ev.value:
            return Number(0)
        return Number(1)


if __name__ == "__main__":
    scope = Scope()
    Read('a').evaluate(scope)
    Read('b').evaluate(scope)

    # Very slow power function
    p = Function(['a', 'b'], [
            Conditional(
                UnaryOperation('!', Reference('b')),
                [Reference('1')],
                [Conditional(
                    BinaryOperation(
                        Reference('b'),
                        '%',
                        Reference('2')
                    ),
                    [BinaryOperation(
                        FunctionCall(Reference('power'), [
                            Reference('a'),
                            BinaryOperation(
                                Reference('b'),
                                '-',
                                Reference('1')
                            )
                        ]),
                        '*',
                        Reference('a')
                    )],
                    [BinaryOperation(
                        FunctionCall(Reference('power'), [
                            Reference('a'),
                            BinaryOperation(
                                Reference('b'),
                                '//',
                                Reference('2')
                            )
                        ]),
                        '*',
                        FunctionCall(Reference('power'), [
                            Reference('a'),
                            BinaryOperation(
                                Reference('b'),
                                '//',
                                Reference('2')
                            )
                        ])
                    )]
                )]
            )
        ])

    # Written by Egor Shcherbin
    gcd = Function(['a', 'b'], [
                    Conditional(
                    UnaryOperation('!',
                                   BinaryOperation(Reference('a'),
                                                   '&&',
                                                   Reference('b'))),
                    [Conditional(Reference('a'),
                                 [Reference('a')],
                                 [Reference('b')])],
                    [Conditional(BinaryOperation(Reference('a'),
                                                 '>=',
                                                 Reference('b')),
                                 [FunctionCall(Reference('gcd'),
                                               [BinaryOperation(
                                                Reference('a'),
                                                '%',
                                                Reference('b')),
                                                Reference('b')])],
                                 [FunctionCall(Reference('gcd'),
                                               [Reference('a'),
                                                BinaryOperation(
                                                Reference('b'),
                                                '%',
                                                Reference('a'))])])])])

    Print(FunctionCall(FunctionDefinition('gcd', gcd),
                        [Reference('a'), Reference('b')])).evaluate(scope)
    Print(FunctionCall(FunctionDefinition('power', p),
                        [Reference('a'), Reference('b')])).evaluate(scope)
