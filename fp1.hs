-- Head
head' :: [a] -> a
head' (x:_) = x

-- Tail
tail' :: [a] -> [a]
tail' [] = []
tail' (_:xs) = xs

-- Take
take' :: Int -> [a] -> [a]
take' 0 x = []
take' n (x:xs) = x : take (n-1) xs

-- Drop
drop' :: Int -> [a] -> [a]
drop' n [] = []
drop' n (x:xs)
    | n == 0 = x:xs
    | otherwise = drop (n-1) (xs)

-- Filter
filter' :: (a -> Bool) -> [a] -> [a]
filter' f [] = []
filter' f (x:xs)
    | f x = x:(filter' f xs)
    | otherwise = filter' f xs

-- Foldl
foldl' :: (a -> b -> a) -> a -> [b] -> a
foldl' f z [] = z
foldl' f z (l:ls) = foldl' f (f z l) ls

-- Concat
concat' :: [a] -> [a] -> [a]
concat' [] y = y
concat' (x:xs) ys = x : concat' xs ys

-- Concat three
concat3 :: [a] -> [a] -> [a] -> [a]
concat3 a b c = concat' (concat' a b) c

-- Quicksort
quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x:xs) =
    concat3 (quicksort [a | a <- xs, a <= x]) [x] (quicksort [a | a <- xs, a > x])

